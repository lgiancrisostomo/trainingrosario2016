﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Practico_1__Ejercicio_5;

namespace Ejercicio_5___Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void isVowelA()
        {
            Assert.IsTrue(Class1.identificarVocal('A'));
        }

        [TestMethod]
        public void isVowela()
        {
            Assert.IsTrue(Class1.identificarVocal('a'));
        }

        [TestMethod]
        public void isVowelB()
        {
            Assert.IsFalse(Class1.identificarVocal('B'));
        }

        [TestMethod]
        public void isVowelb()
        {
            Assert.IsFalse(Class1.identificarVocal('b'));
        }
        [TestMethod]
        public void isVowelU()
        {
            Assert.IsTrue(Class1.identificarVocal('U'));
        }

        [TestMethod]
        public void isVowelu()
        {
            Assert.IsTrue(Class1.identificarVocal('u'));
        }
    }
}
