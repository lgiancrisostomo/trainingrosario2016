﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practico_1__Ejercicio_5
{
    public class Class1
    {
        public static bool identificarVocal(char a)
        {
            var vowels = new List<Char> { 'a', 'e', 'i', 'o', 'u' };
            foreach (var i in vowels)
            {
                if (Char.ToLower(a) == i)
                { return true; }
                               
            }

            return false;
        }
    }
}
