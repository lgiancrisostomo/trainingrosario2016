﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ejercicio2.Imp;


namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void calcularMayor123()
        {
            Assert.AreEqual(3, Ejercicio2.Imp.Ejercicio2.Mayor(1,2,3));

        }

        [TestMethod]
        public void calcularMayor132()

        {
            Assert.AreEqual(3, Ejercicio2.Imp.Ejercicio2.Mayor(1, 3, 2));
        }

        [TestMethod]
        public void calcularMayor312()

        {
            Assert.AreEqual(3, Ejercicio2.Imp.Ejercicio2.Mayor(3, 1, 2));
        }

        [TestMethod]
        public void calcularMenor123()

        {
            Assert.AreEqual(1, Ejercicio2.Imp.Ejercicio2.Menor(1, 2, 3));
        }
        [TestMethod]
        public void calcularMenor213()

        {
            Assert.AreEqual(1, Ejercicio2.Imp.Ejercicio2.Menor(2, 1, 3));
        }
        [TestMethod]
        public void calcularMenor231()

        {
            Assert.AreEqual(1, Ejercicio2.Imp.Ejercicio2.Menor(2,3,1));
        }

        [TestMethod]
        public void calcularPromedio123()
        {
            Assert.AreEqual(2, Ejercicio2.Imp.Ejercicio2.Promedio(1, 2, 3));
        }

        [TestMethod]
        public void calcularPromedio111()
        {
            Assert.AreEqual(1, Ejercicio2.Imp.Ejercicio2.Promedio(1, 1, 1));
        }

        [TestMethod]
        public void calcularPromedio104()
        {
            Assert.AreEqual(1.67, Ejercicio2.Imp.Ejercicio2.Promedio(1, 0, 4));
        }

        [TestMethod]
        public void calcularPromedio104negativo()
        {
            Assert.AreEqual(-1.67, Ejercicio2.Imp.Ejercicio2.Promedio(-1, 0, -4));
        }

    }
}
