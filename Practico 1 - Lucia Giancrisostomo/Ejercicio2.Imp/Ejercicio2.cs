﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2.Imp
{
    public class Ejercicio2
    {
        public static int Mayor(int a, int b, int c)
        {
            if (a >= b)
            {
                if (a >= c)
                {
                    return a;
                }
                else
                {
                    return c;
                }
            }
            else
            {
                if (b >= c)
                {
                    return b;
                }
                else
                {
                    return c;
                }
            }
        }

        public static int Menor(int a, int b, int c)
        {
            if (a<=b)
            {
                if (a <= c)
                {
                    return a;
                }
                else
                {
                    return c;
                }

            } else
            {
                if (b <= c)
                {
                    return b;
                }
                else
                { return c; }
                }
            }
        

        public static double Promedio(int a, int b, int c)
        {
            var prom = new double();
            prom = (a + b + c) / 3d;
            return Math.Round(prom, 2);
        }
    }
}
