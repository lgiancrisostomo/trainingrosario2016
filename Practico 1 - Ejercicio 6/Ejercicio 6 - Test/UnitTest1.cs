﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Practico_1___Ejercicio_6;

namespace Ejercicio_6___Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void multiplicar5x7()
        {
            Assert.AreEqual(35, Class1.multiplicacion(5, 7));
        }
        [TestMethod]
        public void multiplicar0x9()
        {
            Assert.AreEqual(0, Class1.multiplicacion(0, 9));
        }
        [TestMethod]
        public void multiplicar8x0()
        {
            Assert.AreEqual(0, Class1.multiplicacion(8, 0));
        }
        [TestMethod]
        public void multiplicar1x1()
        {
            Assert.AreEqual(1, Class1.multiplicacion(1, 1));
        }

        [TestMethod]
        public void multiplicar1x1Negativo()
        {
            Assert.AreEqual(-1, Class1.multiplicacion(-1, 1));
        }

        [TestMethod]
        public void multiplicar1x1Negativos()
        {
            Assert.AreEqual(1, Class1.multiplicacion(-1, -1));
        }

    }
}

