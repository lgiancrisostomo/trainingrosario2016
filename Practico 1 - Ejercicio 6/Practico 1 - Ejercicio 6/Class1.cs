﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practico_1___Ejercicio_6
{
    public class Class1
    {
        public static int multiplicacion(int v1, int v2)
        {
            var mult = 0;
            if (v1 == 0 || v2 == 0)
            {
                return 0;
            }
            else
            {
                for (int i = 1; i <= Math.Abs(v2); i++)
                {
                    mult = mult + Math.Abs(v1);
                }

                if (v1 < 0 && v2 < 0)
                {
                    return mult;
                }
                else
                {
                    if (v1 < 0 || v2 < 0)
                    { return -mult; }

                    return mult;

                }
            }
        }
    }
}
