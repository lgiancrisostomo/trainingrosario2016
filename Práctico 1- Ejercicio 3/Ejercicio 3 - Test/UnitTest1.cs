﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Práctico_1__Ejercicio_3;

namespace Ejercicio_3___Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void calcularExcedente52()
        {
            Assert.AreEqual(11, Class1.calcularValorExcede(52));
        }

        [TestMethod]
        public void calcularExcedente1()
        {
            Assert.AreEqual(2, Class1.calcularValorExcede(1));
        }
    }
}
