﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios_4_y_5
{
    class Program
    {
        static void Main(string[] args)
        {
            int suma = 0;
            for (int i = 1; i < 26; i++)
            {
                if ((i%2)==0)
                {
                    suma = suma + i;
                }
            }
            Console.WriteLine("La suma de los numeros pares del 1 al 25 es: " + suma);
            Console.WriteLine("Ahora la figura...");
            int contador = 1;
            do
            {
                for (int i = 1; i <= contador; i++)
                {
                    Console.Write("@");
                }
                Console.WriteLine();
                contador += 1;
            } while (contador!=5);
            contador = contador - 2;
            do
            {
                for (int i = 1; i <= contador; i++)
                {
                    Console.Write("@");
                }
                Console.WriteLine();
                contador -= 1;
            } while (contador != 0);
            Console.ReadLine();
        }
    }
}
