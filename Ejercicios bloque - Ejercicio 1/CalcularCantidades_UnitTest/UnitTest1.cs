﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using CalcularCantidades;

namespace CalcularCantidades_UnitTest
{
    [TestClass]
    public class CalcularCantidadesTest
    {
        #region Pares
        [TestMethod]
        public void cantidadNumerosPares13579_0()
        {
            List<int> listatest = new List<int>{1,3,5,7,9};
            Assert.AreEqual(0, Calculos.calcularPares(listatest));
        }

        [TestMethod]
        public void cantidadNumerosPares02468_5()
        {
            List<int> listatest = new List<int> { 0, 2, 4, 6, 8 };
            Assert.AreEqual(5, Calculos.calcularPares(listatest));
        }

        [TestMethod]
        public void cantidadNumerosPares13279_1()
        {
            List<int> listatest = new List<int> { 1, 3, 2, 7, 9 };
            Assert.AreEqual(1, Calculos.calcularPares(listatest));
        }

        [TestMethod]
        public void cantidadNumerosParesNegativos13579_0()
        {
            List<int> listatest = new List<int> { -1, -3, -5, -7, -9 };
            Assert.AreEqual(0, Calculos.calcularPares(listatest));
        }

        [TestMethod]
        public void cantidadNumerosParesNegativos13279_1()
        {
            List<int> listatest = new List<int> { -1, -3, -2, -7, -9 };
            Assert.AreEqual(1, Calculos.calcularPares(listatest));
        }

        [TestMethod]
        public void cantidadNumerosParesNegativosPositivos132789_1()
        {
            List<int> listatest = new List<int> {1, -3, -2, -7, 8, -9 };
            Assert.AreEqual(2, Calculos.calcularPares(listatest));
        }

        [TestMethod]
        public void cantidadNumerosParesNegativosPositivos0132789_1()
        {
            List<int> listatest = new List<int> {0, 1, -3, -2, -7, 8, -9 };
            Assert.AreEqual(3, Calculos.calcularPares(listatest));
        }
        #endregion

        #region Impares
        [TestMethod]
        public void cantidadNumerosImpares2468_0()
        {
            List<int> listatest = new List<int> { 2,4,6,8 };
            Assert.AreEqual(0, Calculos.calcularImpares(listatest));
        }

        [TestMethod]
        public void cantidadNumerosImpares13579_5()
        {
            List<int> listatest = new List<int> { 1, 3, 5, 7, 9 };
            Assert.AreEqual(5, Calculos.calcularImpares(listatest));
        }


        [TestMethod]
        public void cantidadNumerosImparesNegativos13579_5()
        {
            List<int> listatest = new List<int> { -1, -3, -5, -7, -9 };
            Assert.AreEqual(5, Calculos.calcularImpares(listatest));
        }

        [TestMethod]
        public void cantidadNumerosImparesNegativos16278_2()
        {
            List<int> listatest = new List<int> { -1, -6, -2, -7, -8 };
            Assert.AreEqual(2, Calculos.calcularImpares(listatest));
        }

        [TestMethod]
        public void cantidadNumerosImparesNegativosPositivos132789_4()
        {
            List<int> listatest = new List<int> { 1, -3, -2, -7, 8, 9 };
            Assert.AreEqual(4, Calculos.calcularImpares(listatest));
        }
        #endregion
    }
}

