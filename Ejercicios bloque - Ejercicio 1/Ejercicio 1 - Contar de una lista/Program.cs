﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Ejercicio_1___Contar_de_una_lista
{
    class Program
    {
        static void Main(string[] args)
        {
            
            List<int> lista = new List<int>();
            string num;
            do
            {
                Console.WriteLine("Ingrese una lista de numeros, para finalizar presione A");
                num = Console.ReadLine();
                if (num != "A")
                lista.Add(Int32.Parse(num));

            } while (num != "A");

            Console.WriteLine("la cantidad de numeros positivos" + CalcularCantidades.Calculos.calcularNumerosPositivos(lista));
            Console.WriteLine("la cantidad de numeros negativos" + CalcularCantidades.Calculos.calcularNumerosNegativos(lista));
            Console.WriteLine("la cantidad de numeros pares" + CalcularCantidades.Calculos.calcularPares(lista));
            Console.WriteLine("la cantidad de numeros impares" + CalcularCantidades.Calculos.calcularImpares(lista));
            Console.ReadKey();
        }
    }
}
