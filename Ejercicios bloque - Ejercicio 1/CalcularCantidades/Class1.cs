﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcularCantidades
{
    public static class Calculos
    {
        
        public static int calcularNumerosPositivos(List<int> lista)
        {
            int cant = 0;

            foreach (var a in lista)
            {
                if (a >= 0)
                    cant = cant + 1;
            }

            return cant;
        }

        public static int calcularNumerosNegativos(List<int> lista)
        {
            int cant = 0;

            foreach (var a in lista)
            {
                if (a < 0)
                    cant = cant + 1;
            }

            return cant;
        }

        public static int calcularPares(List<int> lista)
        {
            int cant = 0;

            foreach (var a in lista)
            {
                if (a%2 == 0)
                    cant = cant + 1;
            }

            return cant;
        }

        public static int calcularImpares(List<int> lista)
        {
            int cant = 0;

            foreach (var a in lista)
            {
                if (a%2 != 0)
                    cant = cant + 1;
            }

            return cant;
        }
    }
}
