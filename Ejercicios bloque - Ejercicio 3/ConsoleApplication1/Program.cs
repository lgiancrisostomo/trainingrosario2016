﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculos;
namespace ConsoleApplication1
{
    class Program
    {
        private static int nro1;
        private static int nro2;
        static void Main(string[] args)
        {
            
            int opcion = 0;
            do
            {
                Console.WriteLine("Ingrese dos numeros");
                nro1 = int.Parse(Console.ReadLine());
                nro2 = int.Parse(Console.ReadLine());
                Console.WriteLine("1-suma 2-resta 3-producto 4-división 5-salir");
                opcion = int.Parse(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        Console.WriteLine("La suma es: " + Calculos.Calculos.Sumar(nro1, nro2));
                        break;
                    case 2:
                        Console.WriteLine("La resta es: " + Calculos.Calculos.Restar(nro1, nro2));
                        break;
                    case 3:
                        Console.WriteLine("El producto es: " + Calculos.Calculos.Multiplicar(nro1, nro2));
                        break;

                    case 4:
                        Console.WriteLine("La división es: " + Calculos.Calculos.Dividir(nro1, nro2));
                        break;
                }

            } while (opcion!=5);
        }
    }
}
