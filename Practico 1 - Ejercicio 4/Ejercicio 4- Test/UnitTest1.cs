﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Practico_1___Ejercicio_4;

namespace Ejercicio_4__Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void EsPar1()
        {
            Assert.IsFalse(Class1.numeroPar(1));
        }

        [TestMethod]
        public void EsPar2()
        {
            Assert.IsTrue(Class1.numeroPar(2));
        }

        [TestMethod]
        public void EsPar0()
        {
            Assert.IsTrue(Class1.numeroPar(0));
        }

        [TestMethod]
        public void EsPar1Negativo()
        {
            Assert.IsFalse(Class1.numeroPar(-1));
        }

        [TestMethod]
        public void EsPar2Negativo()
        {
            Assert.IsTrue(Class1.numeroPar(-2));
        }
    }
}
