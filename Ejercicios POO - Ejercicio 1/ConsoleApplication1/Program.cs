﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            Auto auto1 = new Auto();
            Console.WriteLine("Ingrese velocidad, marca, modelo, color y patente");
            //auto1.velocidad = Double.Parse(Console.ReadLine());
            auto1.Marca = Console.ReadLine();
            auto1.Modelo = Console.ReadLine();
            auto1.Color = Console.ReadLine();
            auto1.patente = Console.ReadLine();
            auto1.Acelerar(25.50);
            auto1.Frenar(0.50);
            Console.WriteLine("Ingrese velocidad, marca, modelo, color y patente");
            double vel= Double.Parse(Console.ReadLine());
            string mar= Console.ReadLine();
            string mol= Console.ReadLine();
            string col= Console.ReadLine();
            string pat= Console.ReadLine();
            Auto auto2 = new Auto(vel, mar, mol, col, pat);
            auto2.Acelerar(35);
            auto2.Frenar(15);

            Console.WriteLine("Auto 1");
            Console.WriteLine(auto1.Velocidad);
            Console.WriteLine(auto1.Marca);
            Console.WriteLine(auto1.Modelo);
            Console.WriteLine(auto1.Color);
            Console.WriteLine(auto1.patente);

            Console.WriteLine("Auto 2");
            Console.WriteLine(auto2.Velocidad);
            Console.WriteLine(auto2.Marca);
            Console.WriteLine(auto2.Modelo);
            Console.WriteLine(auto2.Color);
            Console.WriteLine(auto2.patente);

            Console.ReadLine();
        }
    }
}
