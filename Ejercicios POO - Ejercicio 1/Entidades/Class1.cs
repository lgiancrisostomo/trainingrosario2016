﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Auto
    {
        private double velocidad=10;
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Color { get; set; }
        private string Patente;

        public double Velocidad
        {
            get
            {
                return velocidad;
            }
        }

        public string patente
        {

            get { return Patente; }

            set { Patente = value; }

        }


        public Auto()
        {
            //naranja.
        }

        public Auto(double vel, string mar, string mod, string col, string pat)
        {
            this.velocidad = vel;
            this.Marca = mar;
            this.Modelo = mod;
            this.Color = col;
            this.Patente = pat;
        }

        public void Acelerar(double incremento)
        {
            velocidad = velocidad + incremento;
        }

        public void Frenar(double decremento)
        {
            velocidad = velocidad - decremento;
        }
    }
}
