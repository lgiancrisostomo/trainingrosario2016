﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Refrigerado : Producto
    {
        private int _codigo_osa;

        public int Codigo_osa {

            get { return _codigo_osa; }
            set { _codigo_osa = value; }
        }

        public Refrigerado(DateTime caducidad, int lote, int codigo_osa): base(caducidad,lote) {
            _codigo_osa = codigo_osa;
        }

        public override void MostrarInfo()
        {
            base.MostrarInfo();
            Console.WriteLine("Código del organismo de supervición alimentaria: " + _codigo_osa);
        }


    }
}
