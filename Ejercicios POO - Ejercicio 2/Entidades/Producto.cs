﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public abstract class Producto
    {
        private DateTime _fecha_caducidad;
        private int _numero_lote;

        public DateTime Fecha_Caducidad
        {
            get { return _fecha_caducidad; }
            set { _fecha_caducidad = value; }
        }

        public int Numero_lote
        {
            get { return _numero_lote; }
            set { _numero_lote = value; }
        }

        public Producto( DateTime caducidad, int lote)
        {
            _fecha_caducidad = caducidad;
            _numero_lote = lote;
        }

        public virtual void MostrarInfo() {
            Console.WriteLine("Fecha Caducidad: " +_fecha_caducidad);
            Console.WriteLine("Número de Lote: " + _numero_lote);
        }

    }
}
