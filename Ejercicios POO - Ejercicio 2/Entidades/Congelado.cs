﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    class Congelado: Producto
    {
        private double _temperatura;

        public double Temperatura {

            get { return _temperatura; }
                set { _temperatura = value; } }

        public Congelado(DateTime caducidad, int lote, double temperatura): base(caducidad,lote) {
            _temperatura = temperatura;
        }

        public override void MostrarInfo()
        {
            base.MostrarInfo();
            Console.WriteLine("Temperatura: " + _temperatura);
        }

    }
}
