﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Frescos : Producto
    {
        private DateTime _fecha_envasado;
        private String _pais_origen;

        public DateTime Fecha_Envasado
        {
            get { return _fecha_envasado; }
            set { _fecha_envasado = value; }
        }

        public String Pais_Origen {
        get { return _pais_origen; }
            set { _pais_origen = value; }
        }

        public Frescos(DateTime caducidad, int lote, DateTime envasado, string origen): base(caducidad, lote) {
            
            _fecha_envasado = envasado;
            _pais_origen = origen; 
        }

        public override void MostrarInfo()
        {
            base.MostrarInfo();
            Console.WriteLine("Fecha de envasado: " + _fecha_envasado);
            Console.WriteLine("Pais de Origen: " + _pais_origen);
        }
    }
}

