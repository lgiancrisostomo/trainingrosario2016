﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculos;


namespace Ejercicio_2
{
    public class Program
    {
        public static int a;
        public static int b;
        public static int c; 

        public static int A
        {
            get { return a; }
            set { a = value; }
        }
        public static int B
        {
            get { return b; }
            set { b = value; }
        }

        public static int C
        {
            get { return c; }
            set { c = value; }
        }
        static void Main(string[] args)
        {
            
            Console.WriteLine("Vas a ingresar 3 numeros");
            Calculos.Calculos.CargarValores( ref a,  ref b, ref c);
            Console.WriteLine("El numero mayor es:"+ Calculos.Calculos.CalcularMayor(ref a, ref b, ref c));
            Console.WriteLine("El numero menor es:"+ Calculos.Calculos.CalcularMenor(ref a, ref b, ref c));
            Console.ReadLine();
        }
    }
}
